script "install_composer" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
  curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
  EOH
  not_if { ::File.exists?("/usr/local/bin/composer") }
end

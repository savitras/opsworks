node[:deploy].each do |application, deploy|
  script "install_composer" do
    interpreter "bash"
    user deploy[:user]
    group deploy[:group]
    cwd "#{deploy[:deploy_to]}/current"
    code <<-EOH
    composer install
    EOH
  end
end
